require 'test/unit'
require_relative './alarm'
require_relative './sensor'

class AlarmTest < Test::Unit::TestCase
  def tests_that_alarm_is_an_Alarm
    alarm = Alarm.new
    assert_equal(Alarm, alarm.class)
  end

  def tests_that_alarm_on_defaults_to_false
    alarm = Alarm.new
    assert_equal(false, alarm.alarm_on)
  end

  def tests_that_Alarm_instance_contains_Sensor_object
    sensor = MockSensor.new(11)
    alarm = Alarm.new(sensor)
    alarm.check
    assert_equal(true, alarm.alarm_on)
  end
end

class MockSensor
    def initialize(number)
        @number = number
    end
    def pop_next_pressure_psi_value
        @number
    end
end