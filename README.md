## Alarm Sensor

## Instructions

Ensure that you have ruby installed by running 

```
$ ruby -v
```

in the terminal

If you don't, visit https://www.ruby-lang.org/en/ for installation instructions.

## Tests

To run the tests, run

```
$ ruby ./alarm_test.rb
```

in the terminal.

## Manual Testing

To use your classes in the terminal, run

```
$ irb
```

in your terminal to open the interactive ruby REPL.

There you can experiment with your classes

```ruby
require "./alarm"
require "./sensor"

sensor = Sensor.new
alarm = Alarm.new(sensor)

alarm.alarm_on
#=> false 

alarm.check
#=> nil
alarm.check
#=> nil
alarm.check
#=> true

alarm.alarm_on
#=> true
```